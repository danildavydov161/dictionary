package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Dictionary letterDictionary = new Dictionary(DictionaryType.LetterDictionary);
        Dictionary numberDictionary = new Dictionary(DictionaryType.NumberDictionary);

        Scanner scanner = new Scanner(System.in);

        String move;
        String key;
        String value;
        boolean active = true;
        boolean active1;

        while(active){
            active1 = true;
            System.out.println("Выберите тип словаря:\n1. Только буквы\n2. Только цифры\n3. Просмотр содержимого словарей\n4. Выход");
            move = scanner.nextLine();
            switch (move){
                case "1":
                    while (active1){
                        System.out.println("Выберите действие:\n1. Просмотр содержимого\n" +
                                "2. Просмотр элемента по ключу\n3. Добавить элемент\n4. Удалить элемент\n5. Вернуться назад\n6. Выход");
                        move = scanner.nextLine();
                        switch (move){
                            case "1":
                                System.out.println(letterDictionary.read());
                                break;
                            case "2":
                                System.out.println("Введите ключ:");
                                key = scanner.nextLine();
                                if(letterDictionary.getDictionary().get(key) == null)
                                    System.out.println("Такого элемента не существует");
                                else
                                    System.out.println(letterDictionary.readByKey(key));
                                break;
                            case "3":
                                System.out.println("Введите ключ:");
                                key = scanner.nextLine();
                                System.out.println("Введите значение:");
                                value = scanner.nextLine();
                                if(letterDictionary.getDictionary().get(key) != null)
                                    System.out.println("Такого элемента уже существует");
                                else {
                                    letterDictionary.write(key, value);
                                    if(letterDictionary.onValid(key + value) == false)
                                        break;
                                    else
                                        System.out.printf("Элемент %s - %s добавлен в словарь\n", key, value);
                                }
                                break;
                            case "4":
                                System.out.println("Введите ключ:");
                                key = scanner.nextLine();
                                if(letterDictionary.getDictionary().get(key) == null)
                                    System.out.println("Такого элемента уже существует");
                                else {
                                    System.out.printf("Элемент %s - %s удален из словаря\n", key, letterDictionary.getDictionary().get(key));
                                    letterDictionary.remove(key);
                                }
                                break;
                            case "5":
                                active1 = false;
                                break;
                            case "6":
                                active = false;
                                break;
                        }
                    }
                    break;
                case "2":
                    while (active1){
                        System.out.println("Выберите действие:\n1. Просмотр содержимого\n" +
                                "2. Просмотр элемента по ключу\n3. Добавить элемент\n4. Удалить элемент\n5. Вернуться назад\n6. Выход");
                        move = scanner.nextLine();
                        switch (move){
                            case "1":
                                System.out.println(numberDictionary.read());
                                break;
                            case "2":
                                System.out.println("Введите ключ:");
                                key = scanner.nextLine();
                                if(numberDictionary.getDictionary().get(key) == null)
                                    System.out.println("Такого элемента не существует");
                                else
                                    System.out.println(numberDictionary.readByKey(key));
                                break;
                            case "3":
                                System.out.println("Введите ключ:");
                                key = scanner.nextLine();
                                System.out.println("Введите значение:");
                                value = scanner.nextLine();
                                if(numberDictionary.getDictionary().get(key) != null)
                                    System.out.println("Такого элемента уже существует");
                                else {
                                    numberDictionary.write(key, value);
                                    if(numberDictionary.onValid(key + value) == false)
                                        break;
                                    else
                                        System.out.printf("Элемент %s - %s добавлен в словарь\n", key, value);
                                }
                                break;
                            case "4":
                                System.out.println("Введите ключ:");
                                key = scanner.nextLine();
                                if(numberDictionary.getDictionary().get(key) == null)
                                    System.out.println("Такого элемента уже существует");
                                else {
                                    System.out.printf("Элемент %s - %s удален из словаря\n", key, numberDictionary.getDictionary().get(key));
                                    numberDictionary.remove(key);
                                }
                                break;
                            case "5":
                                active1 = false;
                                break;
                            case "6":
                                active = false;
                                break;
                        }
                    }
                    break;


                case "3":
                    System.out.println(letterDictionary.read());
                    System.out.println(numberDictionary.read());
                    break;
                case "4":
                    active = false;
                    break;
                default:
                    System.out.println("Неверный ввод");
            }


        }

//        letterDictionary.write("mem", "mim");
//        letterDictionary.write("kuki", "babo");
//        letterDictionary.write("loli", "tyan");
//        letterDictionary.write("boris", "abrakadabra");
//
//        numberDictionary.write("11111", "87567");
//        numberDictionary.write("73423", "33463");
//        numberDictionary.write("456", "72412");
//        numberDictionary.write("45343", "85123");
//        numberDictionary.write("7331867", "45");
//
//        System.out.println();
//
//        System.out.println(letterDictionary.read());
//        System.out.println(numberDictionary.read());
//        System.out.println(letterDictionary.readByKey("kuki"));
//        System.out.println(numberDictionary.readByKey("45343"));
//
//
//        letterDictionary.remove("kuki");
//        numberDictionary.remove("11111");
//
//        System.out.println();
//
//        System.out.println(letterDictionary.read());
//        System.out.println(numberDictionary.read());
//        System.out.println(letterDictionary.readByKey("loli"));
//        System.out.println(numberDictionary.readByKey("45343"));

    }
}

