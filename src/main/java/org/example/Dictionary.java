package org.example;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.*;

class Dictionary {

    private DictionaryType dictionaryType;
    private Map<String, String> dictionary;
    private String fileName;

    public Dictionary(DictionaryType dictionaryType) throws IOException {
        createNewDictionary(dictionaryType);
    }

    public Dictionary(int dictionaryType) throws IOException {
        createNewDictionary(DictionaryType.values()[dictionaryType - 1]);
    }

    private void createNewDictionary(DictionaryType dictionaryType) throws IOException {
        this.dictionaryType = dictionaryType;
        dictionary = new HashMap<>();
        fileName = dictionaryType.toString() + ".txt";

        read();
    }

    public Map<String, String> getDictionary() {
        return dictionary;
    }

    public boolean onValid(String el){

        if(dictionaryType == DictionaryType.LetterDictionary)
            return Pattern.matches("^[A-Za-z]{8}$", el);

        else if(dictionaryType == DictionaryType.NumberDictionary)
            return Pattern.matches("^[0-9]{10}$", el);

        return false;
    }

    public void write(String key, String value) throws Exception {

        if (key.length() != dictionaryType.getWordSize() || value.length() != dictionaryType.getWordSize()) {
            System.out.println(key + " - " + value + ": Неверный размер");
        }
        else if (onValid(key + value)){
            dictionary.put(key, value);
        }

        update();

    }

    public void update(){
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            if(dictionary.size() != 0){
                for(int i = 0; i < dictionary.size(); i++){
                    bw.append(dictionary.keySet().toArray()[i] + " - " + dictionary.get(dictionary.keySet().toArray()[i]) + "\n");
                }
            }

        }
        catch (IOException e) {
            System.out.println(e);
        }
    }

    public Map<String, String> read() throws IOException {
        if (Files.exists(Paths.get(fileName))) {
            BufferedReader br = new BufferedReader(new FileReader(fileName));

            br.lines().forEach(el -> {
                String[] strings = el.split(" - ", 2);
                dictionary.put(strings[0], strings[1]);
            });
        }

        return dictionary;
    }

    public String readByKey(String key) throws IOException {

        read();
        return key + " - " +  dictionary.get(key);
    }

    public void remove(String key) throws Exception {
        dictionary.remove(key);
        update();
    }


}
