package org.example;

enum DictionaryType {
    LetterDictionary(4), NumberDictionary(5);

    private int wordSize;
    DictionaryType(int wordSize){
        this.wordSize = wordSize;
    }

    public int getWordSize(){
        return wordSize;
    }
}
